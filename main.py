import platform , subprocess

# variables globales
version_gs ='gs9.55.0'
pdf_settings = "/ebook"

ruta_archivo_sin_comprimir = "archivo_sin_comprimir.pdf"
ruta_archivo_comprimido = "archivo_comprimido.pdf"

try :
    if( "windows" in platform.system().lower() ):
        proceso = subprocess.Popen([f"C:/Program Files (x86)/gs/{ version }/bin/gswin32c.exe",'-sDEVICE=pdfwrite','-dCompatibilityLevel=1.4',f'-dPDFSETTINGS={ pdf_settings }', '-dNOPAUSE','-dBATCH', '-dQUIET', f"-sOutputFile={ ruta_archivo_comprimido }", ruta_archivo_sin_comprimir],stdout=subprocess.PIPE)            
    else:
        proceso = subprocess.Popen(['/usr/bin/gs', '-sDEVICE=pdfwrite', '-dCompatibilityLevel=1.4', f'-dPDFSETTINGS={ pdf_settings }', '-dNOPAUSE', '-dBATCH',  '-dQUIET',  f'-sOutputFile={ ruta_archivo_comprimido }', ruta_archivo_sin_comprimir], stdout=subprocess.PIPE)
    proceso.wait()
except Exception as e:
    print(f"occurrio el error -> { e } mientras se trataba de comprimir el pdf")
finally :
    print("se comprimio correctamente el pdf")