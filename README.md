# Comprimir PDF

Este repositorio contiene el codígo para comprimir pdf sin perder la calidad,para windows, como para linux.


- [Acerca de](#acerca-de)
- [recursos](#recursos)
- [Instalando GhostScript](#instalando-ghostscript)
    - [Windows](#windows)
    - [Linux](#linux)
- [Ejemplo](#ejemplo)

## Acerca de 

El codígo fue generado para optimizar y reducir el peso de los archivos pdf antes de ser alamacenados en un servidor.

### Recursos

Para poder realizar la funcionalidad de comprimir los archivos pdf, feron necesarios varios recursos como por ejemplo. 

* [GhostScript](https://www.ghostscript.com/)
* [PDF  ejemplo](https://file-examples.com/)
### Instalando GhostScript

Para poder ejecutar el ejemplo en su maquína siga los siguientes pasos.

#### Windows
Paso 1 .- Visitar la siguiente liga [ghostscript.com](https://www.ghostscript.com), como se muestra en la imagen.

![windows](docs/paso-1.png)

Paso 2 .- Hacer click en el botón **download** como en la imagen siguiente.

![windows](docs/paso-2.png)

Paso 3 .- Hacer click en la opción **Ghostscript AGPL Release**  como en la imagen siguiente.

![windows](docs/paso-3.png)

Paso 4 .- Esperar a que se descargue el archivo que se muestra en la sigueinte imagen.

![windows](docs/paso-4.png)
#### Linux
En caso de probar el codígo en sistema linux instalar ghostscript de la siguiente forma.

```shell
pip install ghostscript
```

### Configuración GhostScript


* **/screen** selecciona una salida de baja resolución similar a la configuración de "Pantalla optimizada" de Acrobat Distiller (hasta la versión X).
* **/ebook** selecciona una salida de resolución media similar a la configuración "eBook" de Acrobat Distiller (hasta la versión X).
* **/printer**  selecciona una salida similar a la configuración de "Impresión optimizada" de Acrobat Distiller (hasta la versión X).
* **/prepress** selecciona una salida similar a la configuración "Preimpresión optimizada" de Acrobat Distiller (hasta la versión X).
* **/default** selecciona la salida destinada a ser útil en una amplia variedad de usos, posiblemente a expensas de un archivo de salida más grande.

Visitar para mas detalles [docs gostscript](https://www.ghostscript.com/doc/current/VectorDevices.htm)


## Ejemplo

```python
import platform , subprocess

# variables globales
version_gs = None
pdf_settings = None

ruta_archivo_sin_comprimir = None
ruta_archivo_comprimido = None

try :
    if( "windows" in platform.system().lower() ):
        proceso = subprocess.Popen([f"C:/Program Files (x86)/gs/{ version }/bin/gswin32c.exe",'-sDEVICE=pdfwrite','-dCompatibilityLevel=1.4',f'-dPDFSETTINGS={ pdf_settings }', '-dNOPAUSE','-dBATCH', '-dQUIET', f"-sOutputFile={ ruta_archivo_comprimido }", ruta_archivo_sin_comprimir],stdout=subprocess.PIPE)            
    else:
        proceso = subprocess.Popen(['/usr/bin/gs', '-sDEVICE=pdfwrite', '-dCompatibilityLevel=1.4', f'-dPDFSETTINGS={ pdf_settings }', '-dNOPAUSE', '-dBATCH',  '-dQUIET',  f'-sOutputFile={ ruta_archivo_comprimido }', ruta_archivo_sin_comprimir], stdout=subprocess.PIPE)
    proceso.wait()
except Exception as e:
    print(f"occurrio el error -> { e } mientras se trataba de comprimir el pdf")
finally :
    print("se comprimio correctamente el pdf")
```
